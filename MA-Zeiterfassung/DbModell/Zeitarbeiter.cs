﻿using System;
using System.Collections.Generic;

namespace MA_Zeiterfassung.DbModell
{
    public class Zeitarbeiter
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Kunde Kunde { get; set; }
        public int Gehalt { get; set; }
        public int StundenImMonat { get; set; }
        public string Email { get; set; }
        public string Handynummer { get; set; }

        public ICollection<ZeitarbeiterAuftrag> ZeitarbeiterAuftraege { get; set; }
        public ICollection<Lohn> Loehne { get; set; }

    }
}
