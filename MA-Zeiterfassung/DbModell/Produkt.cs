﻿using System;
using System.Collections.Generic;

namespace MA_Zeiterfassung.DbModell
{
    public class Produkt
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<KundeProdukt> Kunden { get; set; }
    }

}
