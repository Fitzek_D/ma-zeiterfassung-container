﻿using System;
namespace MA_Zeiterfassung.Dto
{
    public class AuftragDto
    {
        public int Nummer { get; set; }
        public string Bezeichnung { get; set; }
        public int Dauer { get; set; }
    }
}
