﻿using System;
namespace MA_Zeiterfassung.Dto
{
    public class ArbeitszeitDto
    {
        public int ArbeiterId { get; set; }
        public int StundenToAdd { get; set; }
    }
}
