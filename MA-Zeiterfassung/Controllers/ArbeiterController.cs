﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MA_Zeiterfassung.Db;
using MA_Zeiterfassung.Dto;
using MA_Zeiterfassung.DbModell;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MA_Zeiterfassung.Controllers
{
    [Route("api/[controller]/[action]")]
    public class ArbeiterController : Controller
    {
        [HttpGet]
        [ActionName("get-arbeiters")]
        public ActionResult<List<ArbeiterDto>> GetArbeiter([FromServices] Database db, int id)
        {
            var arbeiterliste = new List<ArbeiterDto>();

            if (id != 0)
            {
                var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == id);

                var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

                var zeitarbeiter = db.Zeitarbeiter.ToList();


                if (kunde.Zeitarbeiter != null)
                {
                var arbeiter = kunde.Zeitarbeiter.ToList();


                    foreach (var arbeit in arbeiter) { 
                        var arbeiterDto = new ArbeiterDto
                        {
                            Name = arbeit.Name,
                            Id = arbeit.Id,
                            Stunden = arbeit.StundenImMonat
                        };

                        arbeiterliste.Add(arbeiterDto);
                    }

                    return Ok(arbeiterliste);
                }
            }

            return Ok(arbeiterliste);
        }

        [HttpGet]
        [ActionName("get-arbeiter-by-id")]
        public ActionResult<ArbeiterDto> GetArbeiterById([FromServices] Database db, int arbeiterId)
        {
            if (arbeiterId != 0)
            {
                var zeitarbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == arbeiterId);

                var arbeiterDto = new ArbeiterDto
                {
                    Name = zeitarbeiter.Name,
                    Id = zeitarbeiter.Id,
                    Stunden = zeitarbeiter.StundenImMonat
                };

                return Ok(arbeiterDto);
            }

            return Ok();
        }

        [HttpPost]
        [ActionName("add-arbeiter")]
        public ActionResult AddArbeiter([FromBody] ArbeiterCreateDto newArbeiter, [FromServices] Database db)
        {
            var benutzer = db.Benutzer.SingleOrDefault(b => b.Id == newArbeiter.UserId);

            var kunde = db.Kunden.SingleOrDefault(k => k.Id == benutzer.KundeId);

            var arbeiter = new Zeitarbeiter
            {
                Name = newArbeiter.Name,
                Email = newArbeiter.Email,
                Handynummer = newArbeiter.Tel,
                Gehalt = newArbeiter.Gehalt,
                Kunde = kunde,
            };

            db.Zeitarbeiter.Add(arbeiter);
            db.SaveChanges();

            return Ok();
        }

        [HttpPost]
        [ActionName("add-stunden")]
        public ActionResult AddArbeiter([FromBody] ArbeitszeitDto arbeitszeit, [FromServices] Database db)
        {
            var arbeiter = db.Zeitarbeiter.SingleOrDefault(a => a.Id == arbeitszeit.ArbeiterId);

            arbeiter.StundenImMonat += arbeitszeit.StundenToAdd;

            db.Zeitarbeiter.Update(arbeiter);
            db.SaveChanges();

            return Ok();
        }

    }
}
