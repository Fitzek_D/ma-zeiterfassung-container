export interface ProduktDto {
    produktname: string;
    produktlink: string;
    produktIconLink: string;
}