import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AccountIdService } from '../account-id-service';
import { AccountDto } from '../interfaces/AccountDto'
import { ArbeiterDto } from '../interfaces/ArbeiterDto';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  id: number;
  account$ = new Observable<AccountDto>();
  arbeiter$ = new Observable<ArbeiterDto[]>();

  isValid = true;

  constructor(private http: HttpClient, private route: ActivatedRoute, private router: Router, private accountSerevice: AccountIdService) {}

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
        this.id = params['id'];
      });

      console.log('user id: ' + this.id);
      if (this.id){
        console.log('user id ist: ' + this.id);
        this.accountSerevice.setId(this.id);
      }


      this.accountSerevice.userId.subscribe(id => {
        this.id = id;
      })

      console.log('nach  id: ' + this.id);

    if (!this.id){
        window.location.href = 'https://ma-core-container.azurewebsites.net/login';
    }

    this.loadData();
  }

  loadData(){
    this.account$ = this.http.get<AccountDto>("api/benutzer/get-benutzer?id=" + this.id);
    this.arbeiter$ = this.http.get<ArbeiterDto[]>("api/arbeiter/get-arbeiters?id=" + this.id);
  }

  goToArbeiter(arbeiterId: number) {
    const redirect = '/arbeiter';
    this.router.navigate([redirect], {queryParams: {id: arbeiterId}});
  }
}




